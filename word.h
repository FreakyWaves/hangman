/*
 * All about checking
 * and manipulating
 * the hidden word
 */

/*  wordStatus     known chars */
int ws(char *word, char *kc)
{
	unsigned int i = 0;
	unsigned int j = 0;
	unsigned int ok = 0;

	ps("\nMot : ");

	int found = 0;
	while (word[i])
	{
		while (kc[j])
		{
			if (word[i] == kc[j])
			{
				ok++;
				found = 1;
				pc(word[i]);
			}
			j++;
		}
		if (!found)
		{
			pc('*');
		}
		found = 0;
		i++;
		j = 0;
	}
	ps("\n");

	if (ok >= strlen(word) - 1)
		return(1);

	return (0);
}
