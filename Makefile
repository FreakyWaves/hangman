##################################################
#        Makefile for the hangman game           #
#                                                #
#   make run        build & run                  #
#   make clean      clears objects files         #
#   make fclean     clears objects & executable  #
#                                                #
##################################################

CC=gcc
CFLAGS=-g -Wall -Wextra

SRC := $(wildcard *.c)
OBJ := $(SRC:.c=.o)

NAME = HangMan

.PHONY: all

all:link run

run: $(OBJ)
	@echo "\n * * * Executing $(NAME) * * * \n"
	./$(NAME)
#./$(NAME) | cat -An

link : obj $(OBJ)
	@echo "\n = = = Linking into $(NAME) executable = = = \n"
	$(CC) $(CFLAGS) $(OBJ) -o $(NAME)

obj: $(SRC)
	clear
	@echo "\n + + + Creating $(OBJ) files  + + + \n"
	$(CC) $(CFLAGS) -c $(SRC)

clean:
	@echo "\n - - - Removing $(OBJ) files - - - \n"
	@rm $(OBJ)

fclean: clean
	@echo "\n - - - Removing $(NAME) - - - \n"
	@rm $(NAME)

re: fclean all
