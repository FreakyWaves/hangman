/*
 * All about user intraction
 */

#include "io.h"
#include "string.h"

char askUser(char *kc)
{
	static int kci = 0;
	while(1)
	{
		ps("\nEntrez une lettre : \n");
		char ans = gc();

		if (ans < 'A' || ans > 'Z')
		{
			ps("\n! Veuillez entrer une lettre !");
			ans = gc();
		}
		int i = 0;
		int unique = 1;
		while (kc[i])
		{
			if(ans == kc[i])
				unique = 0;
			i++;
		}
		if(unique)
		{
			kc[kci++] = ans;
			return (ans);
		}
		else if(!unique)
			ps("\n! Vous avez deja entre cette lettre !\n");
	}
	return(-1);
}
