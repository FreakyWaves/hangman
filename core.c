/*
 * core
 * manages all
 * the main
 * calls
 */

#include "core.h"
#include "io.h"
#include "userInteract.h"
#include "string.h"
#include "word.h"

void init()
{
	/* Afficher les regles */
	ps("\n");
	ps("# = = Bienvenue au jeu du Pendu = = #\n");
	ps("# Regles :                          #\n");
	ps("#   L'ordinateur choisis un mot     #\n");
	ps("#   et vous devez le deviner        #\n");
	ps("#   mais vous avez un nombre        #\n");
	ps("#   d'essais limite.                #\n");
	ps("# = = Bon amusement = = = = = = = = #\n\n");
}

void play()
{
	char word[BUFMAX] = {0};
	char kc[BUFMAX] = {'0'}; /* known chars */
	int tries = 15;
	char wordFound = 0;

	int replay = 1;
	while(replay)
	{
		wordFound = 0;
		kc[0] = '0';
		int i = 1;
		while(i < BUFMAX)
		{
			kc[i++] = 0;
		}
		init();
		strcpy(word,chooseWord());
		tries = strlen(word) * 2;
		while(!wordFound && tries)
		{
			wordFound = ws(word, kc); /*  wordStatus */
			char ans = askUser(kc);
			tries--;
			ps("\nNombre d'essais restant : ");
			ps(itoa(tries));
			ps("\n");
		}
		replay = end(wordFound, word, kc);
	}
}

int end(int win, char *word, char *kc)
{
	ps("\n\n");
	if(win)
	{
		ws(word, kc);
		ps("\nVous avez gagne");
	}
	else
	{
		ps("\nVous voila pendu");
	}
	ps("\nVoulez-vous rejouer (Y/n)");
	char ans = gc();
	/* ps("ans = '");
	pc(ans);
	ps("'\n"); */

	/*while (ans != 'y' || ans != 'Y' || ans != '\n'||
			ans != 'N' || ans != 'n')
	{
		ps("\n ! REPPONSE INVALIDE ! ");
		ps("\nVoulez-vous rejouer ? (Y/n)\n");
		ans = gc();
	}*/
	if(ans == 'y' || ans == 'Y' || ans == '\n')
	{
		return (1);
	}
	else if (ans == 'n' || ans == 'N')
	{
		return(0);
	}
}

char* chooseWord()
{
	FileStat fs = {0};
	fs = rf("dico");
	ps("Dico : ");
	ps(itoa(fs.lc));
	ps(" lines ");
	ps(itoa(fs.cc));
	ps(" chars\n");
	return ("ABCDEFG");
}
