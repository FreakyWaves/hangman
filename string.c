/*
 * String
 * Manipulating strings
 */
#include <stdio.h>
#include "string.h"
#include "io.h"

unsigned int strlen(char *s)
{
	unsigned int count = 0;
	while(*s)
	{
		count++;
		s++;
	}

	return count;
}

/* copy src string into dst string */
char *strcpy(char *dst, const char *src)
{
	/* check if dst has memory  allocated */
	if (dst == 0)
		return(0);
	/* save address from the start of dst */
	char *ptr = dst;

	while(*src)
	{
		/* assign src to dst */
		*dst++ = *src++;
	}

	/* NULL terminate dst string */
	*dst = '\0';

	/* return address of the start of dst */
	return (ptr);
}

/* capitalize char */
char cc(char c)
{
	if (c >= 'a' && c <= 'z')
		return(c + ('A' - 'a'));
	return(c);
}

/* Copy src into dst but capitalize all chars */
char *strcc(char *dst, char *src)
{
	if (dst == 0)
		return (0);

	char *ptr = dst;

	while (*src)
	{
		*dst++ = cc(*src++);
	}
	*dst = '\0';

	return (ptr);
}

int abs(int i)
{
	return (i < 0 ? -i : -i);
}

void swap(char *x, char *y)
{
	char t = *x;
	*x = *y;
	*y = t;
}

char *strrev(char *buf,int size)
{
	int i = 0;
	while(i < size)
		swap(&buf[i++], &buf[size--]);
	
	return (buf);
}

/* itoa base 10 simplified */
char *itoa(int num)
{
	char buf[BUFF];
	int i = 0;
	const int base = 10;

	if(i < BUFF)
	{
		if (num < 0)
			buf[i++] = '-';

		num = abs(num);
		
		while (num)
		{
			buf[i++] = '0' + (num % base);
			num /= base;
		}
		buf[i] = '\0';
		return (strrev(buf, i - 1));
	}
	ps("\n\n\n ATOI OVERFLOW ERROR \n");
	return (0);
}
