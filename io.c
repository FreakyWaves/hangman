/*
 * All about
 * input
 * &
 * output
 */

#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include "io.h"
#include "string.h"

/*	= = = = = = INPUT = = = = = =	*/

/* read file returns number of lines */
FileStat rf(char *filename)
{
	FileStat fs = {0};
	fs.cc = 0; /* char count */
	fs.lc = 0; /* line count */
	int fd = open(filename, O_RDONLY);
	char buf[BUF];
	
	while(read(fd, &buf,BUF))
	{
		int i = 0;
		while (buf[i])
		{
			if(buf[i] == '\n')
				fs.lc++;
			fs.cc++;
			i++;
		}
	}
	return(fs);
}

/* reads entire file and puts it into an array */
char **ref(char *filename, long size)
{
	char *buf = malloc(sizeof(buf) * size);
	if (buf)
	{
		int fd = open(filename, O_RDONLY);
		read(fd, &buf, size);
		int i = 0;
		while (buf[i])
		{
		/*
		 * split string at '\n'
		 */
		}
	}
	pe("\nNULL MALLOC\n");
	return(0);
}

/* get char from stdin and clear input buffer */
char gc()
{
	ps(" ? : ");
	char c = 0;
	read(1, &c ,1);
	/*ps("[");
	pc(c);
	ps("]\n");*/
	eb();
	return (cc(c));
}

/* clear input buffer */
void eb()
{
	char c = 0;
	while (c != '\n')
		read(1, &c, 1);
}

/*	= = = = = = OUTPUT = = = = = =	*/

/* print char */
void pc(char c)
{
	write(1, &c, 1);
}

/* print newline */
void nl()
{
	ps("\n");
}

/* print string */
void ps(char *s)
{
	while(*s)
		write(1, s++, 1);
}

/* clear screen */
void cs()
{
	ps("\ec");
}

/* print string on stderr */
void pe(char *e)
{
	while (*e)
		write(2, e++, 1);
}
