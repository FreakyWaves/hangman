#ifndef __ioUtils__
#define __ioUtils__

/* Reading */
char gchar();
char *gstr();

/* Writing */
void pchar(char c);
void pstr(char *str);

/* Clearing */

/* 'eat' input buffer */
void eatBuf();
/* Clear console screen */
void clrScr();

#endif
