/* Variables utilities
 *   int to string
 *   reverse buffer
 */
#include <stdio.h>
#define dd printf("[%d] VAR run ok\n",__LINE__);

#include <stdlib.h>
#include "varUtils.h"
#include "gameCore.h"
#include "ioUtils.h"


int abs(int i)
{
	return (i < 0 ? -i : -i);
}

void swap(char *x, char *y)
{
	char t = *x;
	*x = *y;
	*y = t;
}

char *strRev(char *buf,int size)
{
	int i = 0 ;
	while(i < size)
		swap(&buf[i++], &buf[size--]);
	
	return (buf);
}

int strLen(char *str)
{
	int n = 0;
	while(*str++)
		n++;

	return (n);
}

int isMinAlpha(char c)
{
	if(c >= 'a' && c <= 'z')
		return (1);
	
	return(0);
}

char toUpper(char c)
{
	pstr(" ^^^ ToUpper ^^^ ");
	return( c - ('a' + 'A') );
}
char *maximize(char *str, char *buf)
{
	int i = 0;
	if(buf != NULL){
		while(str[i] && buf[i])
		{	
			pstr("\nDebug : str '");
			pstr(str);
			pstr("' buf '");
			pstr(buf);
			pstr("' isMinAlpha [");
			pchar(str[i]);
			pstr("] ? ");
			pchar(isMinAlpha(str[i]) + '0');
			/*       expression     ?     True      : False 
			buf[i] = isMinAlpha(str[i]) ? toUpper(str[i]) : str[i] ;*/
			if(isMinAlpha(str[i]))
				buf[i] = toUpper(str[i]);
			buf[i] = str[i];
			i++;
		}
		buf[i] = '\0';
		return(buf);
	}
	pstr("\n ! ! ! maximize MALLOC NULL ! ! !\n");
	return("\n ! ! ! maximize MALLOC NULL ! ! !\n");
}

/* itoa base 10 simplified */
char *itoa(int num)
{
	char buf[MITOA_BUFF_SIZE];
	int i = 0;
	const int base = 10;

	if(i < MITOA_BUFF_SIZE)
	{
		if (num < 0)
			buf[i++] = '\0';

		num = abs(num);
		
		while (num)
		{
			buf[i++] = '0' + (num % base);
			num /= base;
		}
		buf[i++] = '\0';
		return (strRev(buf,strLen(buf) - 1));
	}
	clrScr();
	pstr("\n\n\n ATOI OVERFLOW ERROR \n");
	return (0);
}
