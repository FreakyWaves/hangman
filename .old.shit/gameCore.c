/*
 * Game Manager
 * Managing the core of the game
 */

#include <stdlib.h>
#include "ioUtils.h"
#include "varUtils.h"
#include "gameCore.h"
#include "word.h"

void gameMngr()
{
	while(play());
}

int play()
{
	char *word;
	word = malloc(sizeof(char) * MAX_WORD_SIZE);
	if (word != NULL)
	{
		word = init();
		start(word);
	}
	else
	{
		clrScr();
		pstr("\n\n ! ! ! ERREUR D ALLOCATION DE MEMOIRE ! ! ! \n\n");
		free(word);
		return(0);
	}
	
	free(word);
	return(0);
}

char *init()
{
	clrScr();
	pstr("\n * * * Bienvenue au jeu du pendu * * * \n\n");
	pstr(" Les regles sont simples :\n");
	pstr(" 1. L'ordinateur choisis un mot\n");
	pstr(" 2. L'Utisateur doit deviner le mot\n");
	pstr("    MAIS son nombre d'essais est limite\n");
	pstr(" Bonne chance ...");
	return(chooseWord());
}


void start(char *word)
{
	char wordMaxed[MAX_WORD_SIZE] = {};
	char END = 0;
	while(!END)
	{
		wordStatus(word);

		pstr("\nword is : ");
		pstr(maximize(word, wordMaxed));
		pstr(wordMaxed);
		pstr("\n");

		END = 1;
	}
}
