#ifndef __gameCore__
#define __gameCore__

#define MAX_WORD_SIZE 256

void gameMngr();

int play();

char *init();

int askUser();

void start();

#endif
