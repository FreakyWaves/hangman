/*
 * + + Hangman Game + +
 *
 * Program chooses
 * a secret word
 * User has to
 * guess it char
 * by char
 * within a limited
 * number of tries
 */

/* V Headers go below V */

#include "gameCore.h"
#include "ioUtils.h"
#include "varUtils.h"

int main()
{
	gameMngr();
	
	return(0);
}
