#ifndef __varUtils__
#define __varUtils__

#define MITOA_BUFF_SIZE 64

void swap(char *x, char *y);

int abs(int i);
int strLen(char *str);
int isMinAlpha(char c);

char toUpper(char c);

char *itoa(int num);
char *strRev(char *buf, int size);
char *maximize(char *str, char *buf);

#endif
