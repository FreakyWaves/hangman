/*
 * INput & OUTput
 * Utilities
 * read & write :
 * char & char[]
 * clear input buffer
 * clear Terminal
 */

#include <unistd.h>

/* Reading */

char gchar()
{
	char c;
	read(1, &c, 1);
	return (c);
}

char *gstr()
{
/* dunno how to implement thiz */
	return("INPUT DUMMY\0");
}

/* Writing */

void pchar(char c)
{
	write(1, &c, 1);
}

void pstr(char *str)
{
	while (*str)
		write(1, str++, 1);
}

/* Clearing */

/* 'eat' input buffer */
void eatBuf()
{
	while (gchar() != '\n');
}

/* Clear console screen */
void clrScr()
{
/* Clear sreen 
 * using ANSI 
 * Escape Sequence
 */
	pstr("\nclrscr\n");
	/*pstr("\ec");*/
}
