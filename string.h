#ifndef __string__
#define __string__

#define BUFF 256

char *strcpy(char *dst, const char *src);
char *strcc(char *dst, char *src);
char *strrev(char *buf,int size);

char cc(char c);
char *itoa(int num);

unsigned int strlen(char *s);
int abs(int i);

void swap(char *x, char *y);

#endif
