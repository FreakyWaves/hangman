#ifndef __io__
#define __io__

#define BUF 4

typedef struct FileStats
{
	int lc; /* line count */
	int cc; /* character count */
} FileStat ; /* file stats */

FileStat rf(char *filename);

char gc();


void eb();
void pc(char c);
void ps(char *s);
void cs();
void pe(char *e);
void nl();

#endif
